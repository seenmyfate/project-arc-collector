require_relative 'lib/project/arc/collector/version'

Gem::Specification.new do |spec|
  spec.name          = 'project-arc-collector'
  spec.version       = Project::Arc::Collector::VERSION
  spec.authors       = ['Tom Clements']
  spec.email         = ['seenmyfate@gmail.com']

  spec.summary       = %q{Reports the results of static analysis tasks to Project Arc}
  spec.description   = %q{Reports the results of static analysis tasks to Project Arc}
  spec.homepage      = 'https://project-arc.herokuapp.com'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'bin'
  spec.executables   = %{project-arc}
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'faraday'
  spec.add_runtime_dependency 'faraday_middleware'
  spec.add_runtime_dependency 'thor'
  spec.add_runtime_dependency 'activesupport'
  spec.add_runtime_dependency 'rubocop'
  spec.add_runtime_dependency 'brakeman'
  spec.add_runtime_dependency 'bundler-audit'
  spec.add_runtime_dependency 'reek'

  spec.add_development_dependency 'vcr'
  spec.add_development_dependency 'webmock'
  spec.add_development_dependency 'dotenv'

end
