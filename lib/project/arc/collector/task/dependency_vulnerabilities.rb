module Project
  module Arc
    module Collector
      class Task
        class DependencyVulnerabilities < Task

          def value
            output, _error, _status = Open3.capture3(
              'bundle exec bundle-audit update && bundle exec bundle-audit check'
            )

            output.scan(/Name:/).size
          end

        end
      end
    end
  end
end
