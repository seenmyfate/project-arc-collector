module Project
  module Arc
    module Collector
      class Task
        class IgnoredSecurityWarnings < Task

          def value
            output, _error, _status = Open3.capture3(
              'bundle exec brakeman . -f json'
            )

            JSON.parse(output).fetch('ignored_warnings').size
          end

        end
      end
    end
  end
end
