module Project
  module Arc
    module Collector
      class Task
        class LinesOfCode < Task

          def name
            'Lines of Code'
          end

          def value
            output, _error, _status = Open3.capture3('bundle exec rake stats')

            last_line = output.split("\n").last
            if match = last_line.match(/Code LOC: (?<loc>\d*)/)
              match[:loc].to_i
            end
          end

        end
      end
    end
  end
end
