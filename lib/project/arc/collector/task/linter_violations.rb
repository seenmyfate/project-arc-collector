require 'open3'
require 'json'
module Project
  module Arc
    module Collector
      class Task
        class LinterViolations < Task

          def value
            output, _error, _status = Open3.capture3('bundle exec rubocop --format json')

            JSON.parse(output).fetch('summary').fetch('offense_count')
          end

        end
      end
    end
  end
end
