module Project
  module Arc
    module Collector
      class Task
        class CodeSmells < Task

          def value
            output, _error, _status = Open3.capture3('bundle exec reek .')

            last_line = output.split("\n").last
            if match = last_line.match(/(?<warnings>\d*) total warnings/)
              match[:warnings].to_i
            end
          end
        end
      end
    end
  end
end
