module Project
  module Arc
    module Collector
      class Task
        class Files < Task

          def value
            output, _error, _status = Open3.capture3('git ls-files | wc -l')

            output.to_i
          end

        end
      end
    end
  end
end
