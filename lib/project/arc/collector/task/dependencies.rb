require 'open3'
require 'json'
module Project
  module Arc
    module Collector
      class Task
        class Dependencies < Task

          def value
            output, _error, _status = Open3.capture3('bundle install')

            target_line = output.split("\n")[-2]
            if match = target_line.match(/(?<gems>\d* gems)/)
              match[:gems].to_i
            end
          end

        end
      end
    end
  end
end
