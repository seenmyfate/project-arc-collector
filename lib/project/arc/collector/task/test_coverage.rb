module Project
  module Arc
    module Collector
      class Task
        class TestCoverage < Task

          def value
            file = File.read('coverage/.last_run.json')

            JSON.parse(file).fetch('result').fetch('covered_percent')
          end

        end
      end
    end
  end
end
