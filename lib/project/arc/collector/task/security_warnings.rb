module Project
  module Arc
    module Collector
      class Task
        class SecurityWarnings < Task

          def value
            output, _error, _status = Open3.capture3(
              'bundle exec brakeman . -f json'
            )

            JSON.parse(output).fetch('warnings').size
          end

        end
      end
    end
  end
end
