module Project
  module Arc
    module Collector
      class Metric

        def initialize(application_id, quality_attribute, value)
          @application_id = application_id
          @quality_attribute = quality_attribute
          @value = value
        end

        def save
          response = API.new(Configuration.instance).save_metric(
            application_id: @application_id,
            quality_attribute_id: quality_attribute_id,
            value: @value
          )
          response.success?
        end

        private

        def quality_attribute_id
          Collector.quality_attribute_registry.fetch(
            @quality_attribute
          ) { Error.new("Unknown quality attribute") }

        end
      end
    end
  end
end
