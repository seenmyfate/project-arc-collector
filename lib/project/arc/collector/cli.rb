require 'thor'
module Project
  module Arc
    module Collector
      class CLI < Thor

        desc "collect APP_ID QUALITY_ATTRIBUTE VALUE", "Collects the new metric for the specific app and quality attribute"
        def collect(app_id, quality_attribute, value)
          Metric.new(app_id, quality_attribute, value).save
        end

        desc "collect_all APP_ID", "Runs the tasks that generate the data Quality Attributes"
        def collect_all(app_id)
          Collector.run(app_id, tasks: Collector.quality_attributes)
        end
      end
    end
  end
end
