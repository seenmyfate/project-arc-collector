require 'singleton'
require 'uri'
module Project
  module Arc
    module Collector
      class Configuration
        include ::Singleton
        attr_writer :domain, :port, :timeout, :open_timeout, :scheme,
          :api_token, :application_id

        def self.configuration
          Configuration.instance
        end

        def self.configure
          yield configuration
        end

        def url
          URI("#{scheme}://#{domain}:#{port}").to_s
        end

        def timeout
          @timeout ||= ENV.fetch('PROJECT_ARC_TIMEOUT', 10).to_i
        end

        def open_timeout
          @open_timeout ||= ENV.fetch('PROJECT_ARC_OPEN_TIMEOUT', 1).to_i
        end

        def domain
          @domain ||= ENV.fetch('PROJECT_ARC_DOMAIN', 'project-arc.herokuapp.com')
        end

        def port
          @port ||= ENV.fetch('PROJECT_ARC_PORT', 443).to_i
        end

        def scheme
          @scheme ||= ENV.fetch('PROJECT_ARC_SCHEME', 'https')
        end

        def api_token
          @api_token || ENV.fetch('PROJECT_ARC_API_TOKEN') { Error.new('api_token must be configured') }
        end

        def application_id
          @application_id || ENV.fetch('PROJECT_ARC_APPLICATION_ID').
            to_i { Error.new('application_id must be configured')}
        end

      end
    end
  end
end
