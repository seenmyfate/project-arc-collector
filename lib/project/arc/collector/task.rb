require 'project/arc/collector/task/linter_violations'
require 'project/arc/collector/task/test_coverage'
require 'project/arc/collector/task/lines_of_code'
require 'project/arc/collector/task/files'
require 'project/arc/collector/task/dependencies'
require 'project/arc/collector/task/dependency_vulnerabilities'
require 'project/arc/collector/task/code_smells'
require 'project/arc/collector/task/security_warnings'
require 'project/arc/collector/task/ignored_security_warnings'
require 'project/arc/collector/task/delivery_lead_time'
require 'project/arc/collector/task/deployment_frequency'
require 'project/arc/collector/task/mean_time_to_restore'
require 'project/arc/collector/task/change_failure_rate'
module Project
  module Arc
    module Collector
      class Task

        def initialize(app_id)
          @app_id = app_id
        end

        def name
          self.class.to_s.demodulize.titleize
        end

        def value
          fail '#value must be implemented'
        end

        def run
          Metric.new(@app_id, name, value).save
        end
      end
    end
  end
end
