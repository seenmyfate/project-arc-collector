require 'faraday'
require 'faraday_middleware'
module Project
  module Arc
    module Collector
      class API
        attr_reader :config

        def initialize(config)
          @config = config
        end

        def save_metric(application_id:, quality_attribute_id:, value:)
          payload = default_payload.merge(
            application_id: application_id,
            quality_attribute_id: quality_attribute_id,
            value: value
          )
          client.post('/api/metrics', payload)
        end

        def applications
          client.get("/api/applications/#{config.application_id}", default_payload)
        end

        private

        def default_payload
          {
            api_token: config.api_token
          }
        end

        def client
          @client ||= Faraday.new(url: config.url) do |builder|
            builder.options[:open_timeout] = config.open_timeout
            builder.options[:timeout] = config.timeout

            builder.use      FaradayMiddleware::EncodeJson
            builder.use      FaradayMiddleware::ParseJson, content_type: 'application/json'

            builder.adapter Faraday.default_adapter
          end
        end

      end
    end
  end
end
