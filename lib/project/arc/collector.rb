require 'project/arc/collector/version'
require 'project/arc/collector/error'
require 'project/arc/collector/configuration'
require 'project/arc/collector/api'
require 'project/arc/collector/metric'
require 'project/arc/collector/cli'
require 'project/arc/collector/task'
require 'active_support/inflector'

module Project
  module Arc
    module Collector
      module_function

      def configure(&block)
        Configuration.configure(&block)
      end

      def run(app_id, tasks: quality_attributes)
        tasks.each do |task|
          Task.const_get(classify(task)).new(app_id).run
        rescue => error
          puts "#{task} failed to run - #{error.inspect}"
        end
      end

      def quality_attributes
        quality_attribute_registry.keys
      end

      def quality_attribute_registry
        @quality_attribute_registry ||= API.new(Configuration.instance).
          applications.body.fetch('quality_attributes').each_with_object({}) do |quality_attribute, registry|
            registry[quality_attribute['name']] = quality_attribute['id']
        end
      end

      def classify(string)
        string.split.map(&:titleize).join
      end
    end
  end
end
