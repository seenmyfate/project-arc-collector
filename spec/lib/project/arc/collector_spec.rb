require 'spec_helper'

module Project
  module Arc
    RSpec.describe Collector do

      describe '.run' do
        subject { Collector.run(1, tasks: ['Linter Violations']) }

        it 'looks up the task class and delegates' do
          expect(Collector::Task::LinterViolations).to receive(:new).with(1)

          subject
        end

        context 'where there is an error' do
          it 'does silences the error' do
            expect(Collector::Task::LinterViolations).to receive(:new).with(1).
              and_raise(StandardError)

            expect { subject }.not_to raise_error
          end
        end
      end

      describe '.quality_attributes_registry' do
        let(:attribute_hash) {
          {
            'Change Failure Rate' => 43,
            'Code Smells' => 48,
            'Delivery Lead Time' => 40,
            'Dependencies' => 47,
            'Dependency Vulnerabilities' => 50,
            'Deployment Frequency' => 41,
            'Files' => 46,
            'Ignored Security Warnings' => 52,
            'Lines of Code' => 45,
            'Linter Violations' => 49,
            'Mean Time to Restore' => 42,
            'Security Warnings' => 51,
            'Test Coverage' => 44
          }
        }

        it 'creates a hash of quality attribute name => id, allowing \
            other classes to look up the name passed as an argument  \
            and validate that ID' do
          VCR.use_cassette('collector/register') do
            expect(Collector.quality_attribute_registry).to eq attribute_hash
          end
        end

        describe '.quality_attributes' do
          let(:attributes_array) {
            [
              'Change Failure Rate',
              'Code Smells',
              'Delivery Lead Time',
              'Dependencies',
              'Dependency Vulnerabilities',
              'Deployment Frequency',
              'Files',
              'Ignored Security Warnings',
              'Lines of Code',
              'Linter Violations',
              'Mean Time to Restore',
              'Security Warnings',
              'Test Coverage'
            ]
          }
          it 'returns an unsorted array of availabile quality attribute names \
              order here is not important but sorting results to ensure a consitent match' do
            VCR.use_cassette('collector/register') do
              expect(Collector.quality_attributes.sort).
                to eq attributes_array.sort
            end
          end
        end
      end
    end
  end
end
