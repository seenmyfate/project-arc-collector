require 'spec_helper'

module Project
  module Arc
    module Collector
      RSpec.describe Metric do
        let(:metric)  { Metric.new(app_id, quality_attribute, value) }
        let(:app_id) { 4 }
        let(:quality_attribute) { 'Lines of Code' }
        let(:value) { 900 }

        it 'takes app ID, quality attribute name and value strings' do
          expect(metric)
        end

        describe '#save' do
          it 'pushes the metric to the application' do
            VCR.use_cassette('metric/save', match_requests_on: [:body]) do
              expect(metric.save).to eq true
            end
          end
        end
      end
    end
  end
end

