require 'spec_helper'

module Project
  module Arc
    module Collector
      class Task
        RSpec.describe Dependencies do
          let(:app_id) { 1 }
          let(:task) { Dependencies.new(1) }

          it 'takes an app id' do
            expect(task)
          end

          it 'defines the quality attribute name' do
            expect(task.name).to eq 'Dependencies'
          end

          describe '#value' do
            let(:output) { File.read('spec/support/bundler_output.txt') }
            let(:error) { '' }
            let(:status) { '' }

            subject { task.value }

            it 'determines a value' do
              expect(Open3).to receive(:capture3).with('bundle install').
                and_return([output, error, status])

              expect(subject).to eq 124
            end
          end
        end
      end
    end
  end
end
