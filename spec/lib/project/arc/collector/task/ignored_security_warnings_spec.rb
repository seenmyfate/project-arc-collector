require 'spec_helper'

module Project
  module Arc
    module Collector
      class Task
        RSpec.describe IgnoredSecurityWarnings do
          let(:app_id) { 1 }
          let(:task) { IgnoredSecurityWarnings.new(1) }

          it 'takes an app id' do
            expect(task)
          end

          it 'defines the quality attribute name' do
            expect(task.name).to eq 'Ignored Security Warnings'
          end

          describe '#value' do
            let(:output) { File.read('spec/support/brakeman_output.json') }
            let(:error) { '' }
            let(:status) { '' }

            subject { task.value }

            it 'determines a value' do
              expect(Open3).to receive(:capture3).with('bundle exec brakeman . -f json').
                and_return([output, error, status])

              expect(subject).to eq 0
            end
          end
        end
      end
    end
  end
end
