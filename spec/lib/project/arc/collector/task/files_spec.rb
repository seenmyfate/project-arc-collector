require 'spec_helper'

module Project
  module Arc
    module Collector
      class Task
        RSpec.describe Files do
          let(:app_id) { 1 }
          let(:task) { Files.new(1) }

          it 'takes an app id' do
            expect(task)
          end

          it 'defines the quality attribute name' do
            expect(task.name).to eq 'Files'
          end

          describe '#value' do
            let(:output) { File.read('spec/support/files_output.txt')}
            let(:error) { '' }
            let(:status) { '' }

            subject { task.value }

            it 'determines a value' do
              expect(Open3).to receive(:capture3).with('git ls-files | wc -l').
                and_return([output, error, status])

              expect(subject).to eq 163
            end
          end
        end
      end
    end
  end
end
