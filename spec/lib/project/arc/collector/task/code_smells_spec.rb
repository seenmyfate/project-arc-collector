require 'spec_helper'

module Project
  module Arc
    module Collector
      class Task
        RSpec.describe CodeSmells do
          let(:app_id) { 1 }
          let(:task) { CodeSmells.new(1) }

          it 'takes an app id' do
            expect(task)
          end

          it 'defines the quality attribute name' do
            expect(task.name).to eq 'Code Smells'
          end

          describe '#value' do
            let(:output) { File.read('spec/support/reek_output.txt') }
            let(:error) { '' }
            let(:status) { '' }

            subject { task.value }

            it 'determines a value' do
              expect(Open3).to receive(:capture3).with('bundle exec reek .').
                and_return([output, error, status])

              expect(subject).to eq 60
            end
          end
        end
      end
    end
  end
end
