require 'spec_helper'

module Project
  module Arc
    module Collector
      class Task
        RSpec.describe LinterViolations do
          let(:app_id) { 1 }
          let(:task) { LinterViolations.new(1) }

          it 'takes an app id' do
            expect(task)
          end

          it 'defines the quality attribute name' do
            expect(task.name).to eq 'Linter Violations'
          end

          describe '#value' do
            let(:output) { File.read('spec/support/rubocop_output.json') }
            let(:error) { '' }
            let(:status) { '' }

            subject { task.value }

            it 'determines a value' do
              expect(Open3).to receive(:capture3).with('bundle exec rubocop --format json').
                and_return([output, error, status])

              expect(subject).to eq 93
            end
          end
        end
      end
    end
  end
end
