require 'spec_helper'

module Project
  module Arc
    module Collector
      class Task
        RSpec.describe TestCoverage do
          let(:app_id) { 1 }
          let(:task) { TestCoverage.new(1) }

          it 'takes an app id' do
            expect(task)
          end

          it 'defines the quality attribute name' do
            expect(task.name).to eq 'Test Coverage'
          end

          describe '#value' do
            let(:file) { File.read('spec/support/simplecov_last_run.json')}

            subject { task.value }

            it 'determines a value' do
              expect(File).to receive(:read).with('coverage/.last_run.json').
                and_return(file)

              expect(subject).to eq 98.31
            end
          end
        end
      end
    end
  end
end
