require 'spec_helper'

module Project
  module Arc
    module Collector
      class Task
        RSpec.describe DependencyVulnerabilities do
          let(:app_id) { 1 }
          let(:task) { DependencyVulnerabilities.new(1) }

          it 'takes an app id' do
            expect(task)
          end

          it 'defines the quality attribute name' do
            expect(task.name).to eq 'Dependency Vulnerabilities'
          end

          describe '#value' do
            let(:output) { File.read('spec/support/bundle_audit_output.txt') }
            let(:error) { '' }
            let(:status) { '' }

            subject { task.value }

            it 'determines a value' do
              expect(Open3).to receive(:capture3).
                with('bundle exec bundle-audit update && bundle exec bundle-audit check').
                and_return([output, error, status])

              expect(subject).to eq 11
            end
          end
        end
      end
    end
  end
end
