require 'spec_helper'

module Project
  module Arc
    module Collector
      RSpec.describe Configuration do
        subject(:configuration) { Configuration.instance }

        it 'has a singleton instance' do
          expect(subject).to_not be_nil
        end

        context 'loading from ENV' do
          it 'returns the url' do
            expect(subject.url).to eq 'http://localhost:3000'
          end

          it 'returns the timeout' do
            expect(subject.timeout).to eq 60
          end

          it 'returns the open_timeout' do
            expect(subject.open_timeout).to eq 20
          end

          it 'returns the api_token' do
            expect(subject.api_token).to eq 'abc123'
          end

          it 'returns the api_token' do
            expect(subject.application_id).to eq 4
          end

        end

        context 'custom configuration' do
          before do
            Collector.configure do |config|
              config.domain = 'localhost'
              config.port = 5000
              config.timeout = 10
              config.open_timeout = 20
              config.scheme = 'http'
              config.api_token = 'api_token_test'
              config.application_id = 1
            end
          end

          it 'returns the url' do
            expect(subject.url).to eq 'http://localhost:5000'
          end

          it 'returns the timeout' do
            expect(subject.timeout).to eq 10
          end

          it 'returns the open_timeout' do
            expect(subject.open_timeout).to eq 20
          end

          it 'returns the api_token' do
            expect(subject.api_token).to eq 'api_token_test'
          end

          it 'returns the api_token' do
            expect(subject.application_id).to eq 1
          end

          after do
            Collector.configure do |config|
              config.domain = nil
              config.port = nil
              config.timeout = nil
              config.open_timeout = nil
              config.scheme = nil
              config.api_token = nil
              config.application_id = nil
            end
          end
        end

      end
    end
  end
end
