require 'spec_helper'

module Project
  module Arc
    module Collector
      RSpec.describe API do
        let(:api) { API.new(Configuration.instance) }

        describe '.new' do
          it 'takes a configuration object' do
            expect(api)
          end
        end

        describe '#save_metric' do
          it 'posts the attribute' do
            VCR.use_cassette('api/save_metric', match_requests_on: [:body], record: :new_episodes) do
              expect(
                api.save_metric(
                  application_id: 4,
                  quality_attribute_id: 45,
                  value: 100,
                )).to be_success
            end
          end
        end

        describe 'applications' do
          it 'makes a request to the applications endpoint' do
            VCR.use_cassette('api/applications') do
              expect(api.applications).to be_success
            end
          end
        end
      end
    end
  end
end
